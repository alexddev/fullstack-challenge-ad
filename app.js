const express = require("express");
const app = express();
const logger = require("./config/logger_config");
const path = require("path");
const api_route = require('./routes/api');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv').config();
const port = process.env.PORT || 3000;

const run = () => {
    try {
        app.use(logger);
        app.use(bodyParser.json())

        var whitelist = ['http://dymbpc7pqtgtg.cloudfront.net/api/get-properties']

        var corsOptionsDelegate = function (req, callback) {
            var corsOptions;
            if (whitelist.indexOf(req.header('Origin')) !== -1) {
                corsOptions = {
                    origin: true
                }
            } else {
                corsOptions = {
                    origin: false
                }
            }
            callback(null, corsOptions)
        }

        app.use(cors())

        app.get("/", function (req, res) {
            res.send("Server");
        });
        app.use(express.static(path.join(__dirname, "./../fullstack-challenge-app")));

        app.use('/api', cors(corsOptionsDelegate), api_route);

        if (process.env.NODE_ENV !== 'test') {
            app.listen(port, () => console.log(`Listening on port ${port}`));
        }

        return app;

    } catch (e) {
        console.log('error is: ', e);
    }

}


module.exports = run();