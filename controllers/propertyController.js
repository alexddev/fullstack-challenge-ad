const {
    insertProperty,
    listProperties,
    getPropertyByLatLng,
    getPropertyBookings
} = require('../services/propertyService');

const addProperty = async (req, res) => {
    try {
        insertProperty(
            req.body.title,
            req.body.address,
            req.body.latitude,
            req.body.longitude
        ).then((data) => {
            res.status(200).send(data);
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}

const getProperties = async (req, res) => {
    try {
        listProperties().then((data) => {
            res.status(200).send(data);
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}

const getPropertyByCoordinates = async (req, res) => {
    try {
        getPropertyByLatLng(req.query.at).then((data) => {
            res.status(200).send(data);
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}

const getBookingsByPropertyId = async (req, res) => {
    try {
        getPropertyBookings(req.params.id).then((data) => {
            res.status(200).send(data);
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}

module.exports = {
    addProperty,
    getProperties,
    getPropertyByCoordinates,
    getBookingsByPropertyId
};