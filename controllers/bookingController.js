const {
    insertBooking
} = require('../services/bookingService');

const addBooking = async (req, res) => {
    try {
        let newBooking = await insertBooking(
            req.body.date,
            req.body.time,
            req.body.property_id
        );

        res.status(200).send(newBooking);
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}


module.exports = {
    addBooking
};