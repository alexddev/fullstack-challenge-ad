'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  db.createTable('bookings', {
    id: {
      type: 'serial',
      primaryKey: true
    },
    property_id: 'int',
    date: 'string',
    time: 'string',
  });
  return null;
};

exports.down = function (db) {
  return db.dropTable('bookings');
};

exports._meta = {
  "version": 1
};