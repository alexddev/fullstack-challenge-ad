const logger = require('morgan');

const config = logger =>
    logger((tokens, req, res) => {
        return [
            tokens.date(req, res),
            tokens.method(req, res),
            tokens.url(req, res),
            tokens.status(req, res),
            tokens.res(req, res, 'content-length'),
            '-',
            tokens['response-time'](req, res),
            'ms'
        ].join(' ');
    });

module.exports = config(logger);