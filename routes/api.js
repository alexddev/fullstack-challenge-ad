const express = require("express");
const path = require("path");
const router = express.Router();

const {
    addProperty,
    getProperties,
    getPropertyByCoordinates,
    getBookingsByPropertyId
} = require('./../controllers/propertyController');

const {
    addBooking,
} = require('./../controllers/bookingController');

/* Property Routes */
router.route('/add-property')
    .post(addProperty);

router.route('/get-properties')
    .get(getProperties);

router.route('/properties')
    .get(getPropertyByCoordinates);

router.route('/properties/:id/bookings')
    .get(getBookingsByPropertyId);

/* Booking Routes */
router.route('/bookings')
    .post(addBooking);

module.exports = router;