import { Component, OnInit, ViewChild, ElementRef, Input, HostListener } from '@angular/core';
import { PropertyService } from '../add-property/property.service';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
declare var H: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {

  private platform: any;
  private ui: any;
  private behavior: any;
  private search: any;
  private map: any;
  private markers: Array<any> = [];

  public query: string;
  public selectedProperty: object;
  public showPropDialog: boolean = false;

  propertyService: PropertyService;
  properties: any;

  @Input()
  public appId: any;

  @Input()
  public appCode: any;

  @Input()
  public lat: any;

  @Input()
  public lng: any;

  @Input()
  public width: any;

  @Input()
  public height: any;

  @ViewChild("map")
  public mapElement: ElementRef;

  public constructor(private http: HttpClient, private router: Router) { }

  public ngOnInit() {
    this.propertyService = new PropertyService(this.http);

    this.getProperties();

    this.selectedProperty = {
      id: '',
      title: '',
      address: ''
    }
  }

  public ngAfterViewInit() {
    this.initMap();
  }

  initMap() {
    const platform = new H.service.Platform({
      apikey: 'QWFsOGNTxQy7xmrXwfHMJZ2Pz3VAvlhYr3y8vrHM6PY',
      useHTTPS: true
    });


    const defaultLayers = platform.createDefaultLayers();

    this.map = new H.Map(
      this.mapElement.nativeElement,
      defaultLayers.raster.normal.map,
      {
        center: { lat: 50, lng: 20 },
        zoom: 4,
        engineType: H.map.render.RenderEngine.EngineType.P2D
      }
    );

    this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
    this.ui = H.ui.UI.createDefault(this.map, defaultLayers);

    return this.map;
  }

  private getProperties() {
    this.properties = this.propertyService.getProperties().subscribe((data) => {
      this.properties = data;
      this.markProperties(data);
    }, (error: HttpErrorResponse) => {
      console.error(error);
    });
  }

  private dropMarker(coordinates: any, data: any) {
    var defaultMarker = new H.map.Icon('assets/images/marker_default.svg');
    let marker = new H.map.Marker(coordinates, { icon: defaultMarker });
    marker.setData("<p data-id='" + data.id + "'>" + data.title + "<br>" + data.address + "</p>");
    marker.addEventListener('tap', event => {
      this.markers.forEach(function (elem, key) {
        elem.setIcon(defaultMarker);
      })

      var activeMarker = new H.map.Icon('assets/images/marker.svg');
      event.target.setIcon(activeMarker);

      this.selectedProperty = {
        id: data.id,
        title: data.title,
        address: data.address
      }
      this.showPropDialog = true;
    }, false);
    this.markers.push(marker);
    this.map.addObject(marker);
  }

  private markProperties(properties: any) {
    console.log(properties);
    for (let i = 0; i < properties.length; i++) {
      let elem = properties[i];
      this.dropMarker({ "lat": elem.latitude, "lng": elem.longitude }, { id: elem.id, title: elem.title, address: elem.address })
    }
  }

  private closeDialog() {
    this.showPropDialog = false;
    this.markers.forEach(function (elem, key) {
      elem.setIcon(new H.map.Icon('assets/images/marker_default.svg'));
    })
  }

  public bookProperty(ev) {
    this.router.navigate(['/add-booking', { prop_id: Number(ev.target.dataset.id) }]);
  }
}