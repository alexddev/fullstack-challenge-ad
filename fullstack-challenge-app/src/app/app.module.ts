import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomepageComponent } from './homepage/homepage.component';
import { MapComponent } from './map/map.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from "@angular/forms";
import { AddPropertyComponent } from './add-property/add-property.component';
import { HttpClientModule } from '@angular/common/http';
import { AddBookingComponent } from './add-booking/add-booking.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    MapComponent,
    HeaderComponent,
    AddPropertyComponent,
    AddBookingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
