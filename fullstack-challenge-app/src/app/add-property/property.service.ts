import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  addPropertySubscription: Subscription;

  constructor(private http: HttpClient) { }

  addProperty(args) {
    return new Observable((observer) => {
      console.log(args);
      this.http.post(`${environment.api_url}/api/add-property`, args).subscribe((data) => {
        observer.next();
      }, (error: HttpErrorResponse) => {
        console.error(error);
      });
    })
  }

  getProperties() {
    return this.http.get(`${environment.api_url}/api/get-properties`);
  }
}
