import { Component, OnInit } from '@angular/core';
import { PropertyService } from './property.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {

  propertyService: PropertyService;
  properties: any;

  getPropertiesSubscription: Subscription;

  title_input: string;
  address_input: string;
  latitude_input: number;
  longitude_input: number;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.propertyService = new PropertyService(this.http);
    this.getProperties();
  }

  addNewProperty(ev) {
    this.propertyService.addProperty({
      title: this.title_input,
      address: this.address_input,
      latitude: this.latitude_input,
      longitude: this.longitude_input
    }).subscribe((data) => {
      this.router.navigateByUrl('/map');
    })
  }

  getProperties() {
    this.properties = this.propertyService.getProperties().subscribe((data) => {
      this.properties = data;
      console.log(data);
    }, (error: HttpErrorResponse) => {
      console.error(error);
    });
    console.log(this.properties);
  }
}
