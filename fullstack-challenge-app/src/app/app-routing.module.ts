import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomepageComponent } from "./homepage/homepage.component";
import { MapComponent } from "./map/map.component";
import { AddPropertyComponent } from "./add-property/add-property.component";
import { AddBookingComponent } from "./add-booking/add-booking.component";

const routes: Routes = [
  { path: "", component: MapComponent },
  { path: "map", component: MapComponent },
  { path: "add-property", component: AddPropertyComponent },
  { path: "add-booking", component: AddBookingComponent },
  { path: "add-booking/:propId", component: AddBookingComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
