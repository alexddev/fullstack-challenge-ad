import { Injectable } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BookingService {
  addBookingSubscription: Subscription;

  constructor(private http: HttpClient) { }

  addBooking(args) {
    return new Observable((observer) => {
      this.http.post(`${environment.api_url}/api/bookings`, args).subscribe((data) => {
        observer.next();
      }, (error: HttpErrorResponse) => {
        console.error(error);
      });
    })
  }

}
