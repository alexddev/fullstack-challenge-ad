import { Component, OnInit } from '@angular/core';
import { PropertyService } from '../add-property/property.service';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { BookingService } from './booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-booking',
  templateUrl: './add-booking.component.html',
  styleUrls: ['./add-booking.component.css']
})
export class AddBookingComponent implements OnInit {

  propertyService: PropertyService;
  bookingService: BookingService;
  properties: any;

  date: string;
  time: string;
  property_id = 1;
  prop_id: number;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.propertyService = new PropertyService(this.http);
    this.bookingService = new BookingService(this.http);
    this.getProperties();
    this.route.paramMap.subscribe(params => {
      this.prop_id = +params.get('propId');
      console.log(this.prop_id);
    });
  }

  addNewBooking(ev) {
    this.bookingService.addBooking({
      date: this.date,
      time: this.time,
      property_id: this.property_id
    }).subscribe((data) => {
      this.router.navigateByUrl('/map');
    })
  }

  getProperties() {
    this.properties = this.propertyService.getProperties().subscribe((data) => {
      this.properties = data;
    }, (error: HttpErrorResponse) => {
      console.error(error);
    });
  }

  getData(event) {
    let selectElem = event.target;
    let selectedOption = selectElem.options[selectElem.selectedIndex];
    this.property_id = selectedOption.dataset.id;
  }
}
