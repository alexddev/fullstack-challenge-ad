import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public expanded = false;
  constructor() { }

  ngOnInit() {
  }

  expandMobileMenu(ev) {
    this.expanded = this.expanded ? false : true;
  }
}
