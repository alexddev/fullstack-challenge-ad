# Fullstack Challenge

**Front-end:**
Two forms to add properties / bookings.
Map with markers on the added properties.


**Back-end API routes:**

`/api/properties?at=LAT,LONG`	//Returns all properties around Lat/Lon (accuracy ~4)

`/api/bookings`	//Creates a booking for a property with a post request, it is used on the /add-booking client-side page

`/api/properties/PROPERTY_ID/bookings`	//Returns the bookings for a property

(Back end logic is set up, although two of them are not used in the front end.)

### Hosting
* Uploaded Angular server on S3
* Created Cloudfront distribution for the bucket
* Uploaded Node server on EC2 Server
* Started the server on PM2
* Created a database using AWS RDS Service
* Ran migrations live

http://dymbpc7pqtgtg.cloudfront.net/ Should be opened with http

Hosting - 2h. 

Fixes - 2h.

Total development time: 12 hours