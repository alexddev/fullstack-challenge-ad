const db = require('../db/pg');

const insertBooking = async (date, time, property_id) => {
    return new Promise(function (resolve, reject) {
        const query = {
            text: 'INSERT INTO bookings(date, time, property_id) VALUES($1, $2, $3) RETURNING id, date, time, property_id',
            values: [date, time, property_id],
        }
        // callback
        db.query(query, (err, res) => {
            if (err) {
                reject(err);
                throw new Error(err);
            } else {
                resolve(res.rows);
            }
        })
    })
}

module.exports = {
    insertBooking
}