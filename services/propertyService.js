const db = require('../db/pg');

const insertProperty = async (title, address, long, lat) => {
    return new Promise(function (resolve, reject) {
        const query = {
            text: 'INSERT INTO properties(title, address, latitude, longitude) VALUES($1, $2, $3, $4) RETURNING id, title, address, latitude, longitude',
            values: [title, address, long, lat],
        }
        // callback
        db.query(query, (err, res) => {
            if (err) {
                reject(err);
                throw new Error(err);
            } else {
                resolve(res.rows);
            }
        })
    })
}

const listProperties = async () => {
    return new Promise(function (resolve, reject) {
        const query = {
            text: 'SELECT * FROM properties',
        }
        // callback
        db.query(query, (err, res) => {
            if (err) {
                reject(err);
                throw new Error(err);
            } else {
                resolve(res.rows);
            }
        })
    })
}

const getPropertyByLatLng = async (coordinates) => {
    return new Promise(function (resolve, reject) {
        let lat = coordinates.split(',')[0];
        let lng = coordinates.split(',')[1];

        let lat_from = Number(lat) - 4,
            lat_to = Number(lat) + 4,
            lng_from = Number(lng) - 4,
            lng_to = Number(lng) + 4;

        const query = {
            text: 'SELECT * FROM properties WHERE (latitude BETWEEN $1 AND $2) AND (longitude BETWEEN $3 AND $4)',
            values: [lat_from, lat_to, lng_from, lng_to]
        }

        // callback
        db.query(query, (err, res) => {
            if (err) {
                reject(err);
                throw new Error(err);
            } else {
                resolve(res.rows);
            }
        })
    });
}

const getPropertyBookings = async (id) => {
    return new Promise(function (resolve, reject) {
        const query = {
            text: 'SELECT * FROM bookings WHERE property_id=$1',
            values: [id]
        }
        // callback
        db.query(query, (err, res) => {
            if (err) {
                reject(err);
                throw new Error(err);
            } else {
                resolve(res.rows);
            }
        })
    });
}

module.exports = {
    insertProperty,
    listProperties,
    getPropertyByLatLng,
    getPropertyBookings
}