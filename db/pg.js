const {
    DB_LOCAL_URI
} = require('../config/env_config');

const {
    Client
} = require('pg')

const client = new Client(DB_LOCAL_URI);

client.connect(err => {
    if (err) {
        console.error('connection error', err.stack)
    } else {
        console.log('connected')
    }
});

module.exports = client;