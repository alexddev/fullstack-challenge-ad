const db = require("../db/pg");
const request = require("supertest");
const app = require("../app");

var test_properties = [];

beforeAll(async () => {
    let insertResult = await db.query("INSERT INTO properties (title, address, latitude, longitude) VALUES ('Test prop 1', 'Test address 1', 2000, 2000) RETURNING id");
    test_properties.push(insertResult.rows[0].id);
});

beforeEach(async () => {

});

afterEach(async () => {

});

afterAll(async () => {
    await db.query("DELETE FROM properties WHERE id = ANY ($1)", [test_properties]);
    db.end();
});

describe("TEST Property CREATE", () => {
    test("Property inserted correctly", async () => {
        const newProperty = await request(app)
            .post("/api/add-property")
            .send({
                title: 'Test prop 2',
                address: 'Test address 2',
                latitude: 2000,
                longitude: 2000
            });

        expect(newProperty.body[0]).toHaveProperty("id");
        expect(newProperty.body[0].title).toBe("Test prop 2");
        expect(newProperty.body[0].address).toBe("Test address 2");
        expect(newProperty.body[0].latitude).toBe("2000");
        expect(newProperty.body[0].longitude).toBe("2000");
        expect(newProperty.statusCode).toBe(200);

        test_properties.push(newProperty.body[0].id);
    });
});

describe("TEST Property GET", () => {
    test("All properties retrieved correctly", async () => {
        const getProperties = await request(app).get("/api/properties?at=1999,2001");

        expect(getProperties.body.length).toBe(2);
        expect(getProperties.body[0]).toHaveProperty("id");
        expect(getProperties.body[0]).toHaveProperty("title");
        expect(getProperties.body[0]).toHaveProperty("latitude");
        expect(getProperties.body[0]).toHaveProperty("longitude");
        expect(getProperties.statusCode).toBe(200);
    });
});