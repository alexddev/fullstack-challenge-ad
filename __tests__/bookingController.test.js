const db = require("../db/pg");
const request = require("supertest");
const app = require("../app");

var test_property_id;
var test_booking_id;

beforeAll(async () => {
    let insertResult = await db.query("INSERT INTO properties (title, address, latitude, longitude) VALUES ('Test prop 1', 'Test address 1', 1000, 1000) RETURNING id");
    test_property_id = insertResult.rows[0].id;
});

beforeEach(async () => {

});

afterEach(async () => {

});

afterAll(async () => {
    await db.query("DELETE FROM properties WHERE id = $1", [test_property_id]);
    await db.query("DELETE FROM bookings WHERE id = $1", [test_booking_id]);
    db.end();
});

describe("TEST Booking CREATE", () => {
    test("Booking inserted correctly", async () => {
        const newBooking = await request(app)
            .post("/api/bookings")
            .send({
                date: '2020-12-12',
                time: '00:00',
                property_id: test_property_id
            });

        test_booking_id = newBooking.body[0].id;

        expect(newBooking.body[0]).toHaveProperty("id");
        expect(newBooking.body[0].date).toBe("2020-12-12");
        expect(newBooking.body[0].time).toBe("00:00");
        expect(newBooking.body[0].property_id).toBe(test_property_id);
        expect(newBooking.statusCode).toBe(200);
    });
});

describe("TEST Retrieve bookings for property", () => {
    test("Bookings retrieved correctly", async () => {
        const getBookings = await request(app).get("/api/properties/" + test_property_id + "/bookings");

        expect(getBookings.body.length).toBe(1);
        expect(getBookings.body[0].date).toBe("2020-12-12");
        expect(getBookings.body[0].time).toBe("00:00");
        expect(getBookings.body[0].property_id).toBe(test_property_id);
        expect(getBookings.statusCode).toBe(200);
    });
});